using System;
using System.IO;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Test;
using Worker1;

namespace Tests
{
    public class MSSQLDataTimeRepositoryFixture : IDisposable
    {
        private EnvironmentController _environmentController = new EnvironmentController();

        public MSSQLDateTimeRepository Repository { get; }

        public MSSQLDataTimeRepositoryFixture()
        {
            _environmentController.Setup();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Integration.json", optional: true, reloadOnChange: true);

            var databaseConnectionString = _environmentController.ConnectionStrings.First(c => c.Identifier.Equals("mssql"));
            var configuration = builder.Build();

            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
            connectionStringBuilder.Password = databaseConnectionString.Password;
            connectionStringBuilder.UserID = databaseConnectionString.Username;
            connectionStringBuilder.DataSource = $"{databaseConnectionString.Host},{databaseConnectionString.FirstPort}";
            connectionStringBuilder.TrustServerCertificate = true;

            var databaseOptions = new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString };
            Repository = new MSSQLDateTimeRepository(databaseOptions);
        }

        public void Clean()
        {
            _environmentController.Clean();
        }

        public void Dispose()
        {
            _environmentController.Dispose();
        }
    }
}

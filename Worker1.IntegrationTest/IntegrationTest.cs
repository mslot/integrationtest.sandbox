using System.Threading.Tasks;
using Worker1;
using Test;
using Xunit;
using Microsoft.Extensions.Hosting;
using System;
using MassTransit;
using Core;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Security.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.SqlClient;

namespace Tests
{
    public class IntegrationTest
        : IClassFixture<CustomGenericHostApplicationFactory<Program>>
    {
        private readonly CustomGenericHostApplicationFactory<Program> _factory;

        public IntegrationTest(CustomGenericHostApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task SaveOneTimeStamp()
        {
            // we find the correct connectionstring
            var messageQueueConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("rabbitmq"));
            var databaseConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("mssql"));

            _factory.StartHost((builder) =>
            {
                builder.UseEnvironment("Integration");
                builder.ConfigureAppConfiguration((context, configuration) =>
                {
                    // we inject rabbitmq into the configuration
                    // this needs to be done this way, because MassTransit is set up in Startup, and therefore doesnt get injected into the IoC
                    configuration.AddInMemoryCollection(new Dictionary<string, string>
                    {
                        {"MessageQueueOptions:Username",messageQueueConnectionString.Username},
                        {"MessageQueueOptions:Password",messageQueueConnectionString.Password},
                        {"MessageQueueOptions:Protocol",messageQueueConnectionString.Protocol},
                        {"MessageQueueOptions:Host",messageQueueConnectionString.Host},
                        {"MessageQueueOptions:Path",messageQueueConnectionString.Path},
                        {"MessageQueueOptions:Port",messageQueueConnectionString.FirstPort.ToString()}
                    });
                });
                builder.ConfigureServices((services) =>
                {
                    // we need to remove the database options that has been setup by the apis Startup
                    var databaseOptionsServiceDescriptor = services.FirstOrDefault(descriptor => descriptor.ServiceType == typeof(DatabaseOptions));
                    services.Remove(databaseOptionsServiceDescriptor);

                    // we add our own database that has been handed to us by the environment controller
                    // we REMEMBER to set the correct initial catalog - in environment controller land we were on master, because it needs to create the database
                    var connectionStringBuilder = new SqlConnectionStringBuilder();
                    connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
                    connectionStringBuilder.Password = databaseConnectionString.Password;
                    connectionStringBuilder.UserID = databaseConnectionString.Username;
                    connectionStringBuilder.DataSource = $"{databaseConnectionString.Host},{databaseConnectionString.FirstPort}";
                    connectionStringBuilder.TrustServerCertificate = true;

                    services.AddSingleton(new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString });
                });

            });

            // we need a bus so we can create a client that talks for with the bus
            string host = $"rabbitmq://{messageQueueConnectionString.Host}:{messageQueueConnectionString.FirstPort}";
            var serviceAddress = new Uri($"{host}/Core:DateTimeStampMessage");

            var bus = Bus.Factory.CreateUsingRabbitMq((cfg) =>
            {
                if (!String.IsNullOrEmpty(messageQueueConnectionString.Path))
                {
                    host = $"{host}{messageQueueConnectionString.Path}";
                }

                cfg.Host(host, h =>
                {
                    h.Password(messageQueueConnectionString.Password);
                    h.Username(messageQueueConnectionString.Username);

                    if (messageQueueConnectionString.Protocol.Equals("tls12", StringComparison.InvariantCulture))
                    {
                        h.UseSsl(s =>
                        {
                            s.Protocol = SslProtocols.Tls12;
                        });
                    }
                });
            });

            bus.Start();
            var client = bus.CreateRequestClient<DateTimeStampMessage>(serviceAddress);

            var timestamp = DateTimeOffset.Now;
            var response = await client.GetResponse<DateTimeResponse>(new DateTimeStampMessage(timestamp));
            Assert.Equal(timestamp, response.Message.TimeStamp);

            bus.Stop();
        }
    }
}

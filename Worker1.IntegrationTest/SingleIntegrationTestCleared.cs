
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Test;
using Worker1;
using Xunit;
using Xunit.Extensions.Ordering;

// This is for integration testing single items
// (fx when you can do a fully integration test for an azure function but you still want to integration test some parts of the
//       function that can't be covered by an unit test)
namespace Tests
{
    // as you can see here, the environment is shared among both tests
    // the test in a class fixture run sequential as explained here: https://xunit.net/docs/running-tests-in-parallel
    // quote: "By default, each test class is a unique test collection. Tests within the same test class will not run in parallel against each other."
    //
    // I have added ordering with this extension: https://www.nuget.org/packages/Xunit.Extensions.Ordering because we cant ourself be sure on the ordering that
    // xunit provides: they don't tell us if it is consistent. We need ordering to be sure our assert on the all count is correct.
    public class MSSQLDateTimeRepositoryTestCleared : IDisposable, IClassFixture<MSSQLDataTimeRepositoryFixture>
    {
        private readonly MSSQLDataTimeRepositoryFixture _fixture;
        public MSSQLDateTimeRepositoryTestCleared(MSSQLDataTimeRepositoryFixture fixture)
        {
            _fixture = fixture;
        }

        // To only run this test you can use "dotnet test --filter SaveTimeStamp"
        [Fact, Order(1)]
        public void SaveTimeStamp()
        {
            DateTimeOffset now = DateTimeOffset.Now;
            var returned = _fixture.Repository.Upsert(now);
            var all = _fixture.Repository.GetAll();

            Assert.Equal(now, returned);
            Assert.Equal(1, all.Count());
        }

        [Fact, Order(2)]
        public void SaveTimeStampAgain()
        {
            DateTimeOffset now = DateTimeOffset.Now;
            var returned = _fixture.Repository.Upsert(now);
            var all = _fixture.Repository.GetAll();

            Assert.Equal(now, returned);
            Assert.Equal(1, all.Count());
        }

        public void Dispose()
        {
            _fixture.Clean(); //Cleans up after each test
        }
    }
}

-- evolve-tx-off
IF NOT EXISTS (
        SELECT *
        FROM sys.databases
        WHERE name = 'TimeStampDatabase'
        )
BEGIN
    CREATE DATABASE [TimeStampDatabase]
END

-- we need the first comment because of this: https://github.com/lecaillon/Evolve/issues/63. This is covered in detail here: https://evolve-db.netlify.app/faq/#i-classfas-fa-checki-can-i-control-how-evolve-manages-sql-transactions-

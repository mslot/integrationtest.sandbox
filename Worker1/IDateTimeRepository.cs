using System;
using System.Collections.Generic;

namespace Worker1
{
    public interface IDateTimeRepository
    {
        IEnumerable<DateTimeOffset> GetAll();
        DateTimeOffset Upsert(DateTimeOffset timestamp);
    }
}

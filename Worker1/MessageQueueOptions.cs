namespace Worker1
{
    public class MessageQueueOptions
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Protocol { get; set; }
        public string Host { get; set; }
        public string Path { get; set; }
        public int Port { get; set; }
    }
}

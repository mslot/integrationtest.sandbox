using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using System.Security.Authentication;

namespace Worker1
{
    public class Program
    {
        // See more here for how to set up a workload: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-5.0#set-up-a-host
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    services.AddScoped<IDateTimeRepository, MSSQLDateTimeRepository>();

                    // we do it this way so we don't get an extra dependency on IOptions assembly everywhere
                    var messageQueueOptions = new MessageQueueOptions();
                    DatabaseOptions databaseOptions = new DatabaseOptions();

                    var messageQueueOptionsSection = configuration.GetSection(nameof(MessageQueueOptions));
                    var databaseSection = configuration.GetSection(nameof(DatabaseOptions));

                    messageQueueOptionsSection.Bind(messageQueueOptions);
                    databaseSection.Bind(databaseOptions);

                    services.AddSingleton(databaseOptions);

                    services.AddMassTransit(x =>
                    {
                        x.AddConsumer<TimeStampConsumer>();
                        x.UsingRabbitMq((context, cfg) =>
                        {
                            string host = $"rabbitmq://{messageQueueOptions.Host}:{messageQueueOptions.Port}";

                            if (!String.IsNullOrEmpty(messageQueueOptions.Path))
                            {
                                host = $"{host}{messageQueueOptions.Path}";
                            }

                            cfg.ConfigureEndpoints(context);

                            cfg.Host(host, h =>
                            {
                                h.Password(messageQueueOptions.Password);
                                h.Username(messageQueueOptions.Username);

                                if (messageQueueOptions.Protocol.Equals("tls12", StringComparison.InvariantCulture))
                                {
                                    h.UseSsl(s =>
                                    {
                                        s.Protocol = SslProtocols.Tls12;
                                    });
                                }
                            });
                        });
                    });

                    services.AddHostedService<MassTransitConsoleHostedService>();
                });
    }
}

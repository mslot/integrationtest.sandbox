using System;
using System.Threading.Tasks;
using Core;
using MassTransit;

namespace Worker1
{
    public class TimeStampConsumer : IConsumer<DateTimeStampMessage>
    {
        private IDateTimeRepository _repository;
        public TimeStampConsumer(IDateTimeRepository repository)
        {
            _repository = repository;
        }

        public async Task Consume(ConsumeContext<DateTimeStampMessage> context)
        {
            System.Console.WriteLine("Consumed stamp");
            var timestamp = context.Message.Stamp;
            var savedTimeStamp = _repository.Upsert(timestamp);
            await context.RespondAsync(new DateTimeResponse(savedTimeStamp));
        }
    }
}

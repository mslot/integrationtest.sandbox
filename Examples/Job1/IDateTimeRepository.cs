using System;
using System.Collections.Generic;

namespace Examples.Job1
{
    public interface IDateTimeRepository
    {
        IEnumerable<DateTimeOffset> GetAll();
        DateTimeOffset Upsert(DateTimeOffset timestamp);
    }
}

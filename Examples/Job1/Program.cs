using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Examples.Job1
{
    // For now this can't be unit tested with WebApplicationFactory. Microsoft recommend to do the same as their EndToEnd tests, referenced here: https://github.com/Azure/azure-webjobs-sdk/issues/2310
    // This is the same as Azure Functions (until they have an inmemory test ready)
    //
    // I have created my own host factory, to test this. See the integrationTest for this job
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebJobs(b =>
                {
                    b.AddAzureStorageCoreServices()
                        .AddAzureStorage()
                        .AddHttp();
                })
                .ConfigureServices((context, services) =>
                {
                    var configuration = context.Configuration;
                    var databaseOptions = new DatabaseOptions();
                    var databaseSection = configuration.GetSection(nameof(DatabaseOptions));

                    services.AddSingleton<IDateTimeRepository, MSSQLDateTimeRepository>();
                    services.AddSingleton(databaseOptions);
                })
                .ConfigureLogging((context, b) =>
                {
                    b.SetMinimumLevel(LogLevel.Debug);
                    b.AddConsole();
                })
                .ConfigureAppConfiguration((context, configuration) =>
                {
                    // We need to do this because ConfigureWebJobs is messing with the configuration sources,
                    // adding appsettings.json. Read more here: https://github.com/Azure/azure-webjobs-sdk/issues/1931
                    configuration.Sources.Clear();
                    configuration
                        .AddJsonFile("appsettings.json", optional: true)
                        .AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true);
                    configuration.AddEnvironmentVariables();

                    if (args.Any())
                    {
                        configuration.AddCommandLine(args);
                    }
                })
                .UseConsoleLifetime();
    }
}

using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace Examples.Job1.Functions
{
    public class Functions
    {
        private IDateTimeRepository _repository;
        public Functions(IDateTimeRepository repository)
        {
            _repository = repository;
        }

        public void QueueTriggerTest(
            [QueueTrigger("queue")]
            string item,
            ILogger log)
        {
            _repository.Upsert(DateTimeOffset.Now);
        }

        // Muted for now until this gets answered: https://stackoverflow.com/questions/68889706/azure-webjobs-sdk-httptrigger-what-is-the-default-url-on-localhost
        // It seems that there might be some config that i have missed, but the docs doesn't mention how to set this up, so i can't call the endpoint
        //[FunctionName("httptriggertest")]
        //public async Task<IActionResult> HttpTriggerTest([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "httptriggertest")] HttpRequest req,
        //ILogger log)
        //{
        //log.Log(LogLevel.Information, "Received request request");
        //
        //return new OkObjectResult("HttpTriggerTest");
        //}
    }
}

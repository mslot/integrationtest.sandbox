using System.Threading.Tasks;
using Examples.Job1;
using Xunit;
using Microsoft.Extensions.Hosting;
using System;
using Azure.Storage.Queues;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Test;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace Examples.Test
{
    public class IntegrationTest
        : IClassFixture<CustomGenericHostApplicationFactory<Program>>
    {
        private readonly CustomGenericHostApplicationFactory<Program> _factory;

        public IntegrationTest(CustomGenericHostApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task CallQueueTrigger()
        {
            var azureStorageConnection = _factory.ConnectionStrings.First(c => c.Identifier.Equals("azureStorage"));
            var databaseConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("mssql"));
            string connectionString = $"DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;QueueEndpoint=http://127.0.0.1:{azureStorageConnection.Ports["queue"]}/devstoreaccount1;BlobEndpoint=http://127.0.0.1:{azureStorageConnection.Ports["blob"]}/devstoreaccount1";

            // we add our own database that has been handed to us by the environment controller
            // we REMEMBER to set the correct initial catalog - in environment controller land we were on master, because it needs to create the database
            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
            connectionStringBuilder.Password = databaseConnectionString.Password;
            connectionStringBuilder.UserID = databaseConnectionString.Username;
            connectionStringBuilder.DataSource = $"{databaseConnectionString.Host},{databaseConnectionString.FirstPort}";

            string queueName = "queue";

            _factory.StartHost((configuration) =>
            {
                configuration.UseEnvironment("integration");
                configuration.ConfigureAppConfiguration((context, configuration) =>
                {
                    // we need to inject this in the config so azure sdk can use it
                    configuration.AddInMemoryCollection(new Dictionary<string, string>
                    {
                        {"ConnectionStrings:AzureWebJobsStorage",connectionString}
                    });
                });
                configuration.ConfigureServices((context, services) =>
                {
                    // we need to remove the database options that has been setup by the apis Startup
                    var databaseOptionsServiceDescriptor = services.FirstOrDefault(descriptor => descriptor.ServiceType == typeof(DatabaseOptions));
                    services.Remove(databaseOptionsServiceDescriptor);

                    services.AddSingleton(new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString });
                });
            });

            // if not the QueueClientOptions is present doing encoding, we get an error. Exactly what we want with an integration test :)
            QueueClient queueClient = new QueueClient(connectionString, queueName, new QueueClientOptions { MessageEncoding = QueueMessageEncoding.Base64 });

            queueClient.CreateIfNotExists();

            queueClient.SendMessage("message");

            // We need to wait a bit because of how the storage queue polling works
            await Task.Delay(TimeSpan.FromSeconds(1));

            var repository = new MSSQLDateTimeRepository(new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString });

            var all = repository.GetAll();

            Assert.Equal(1, all.Count());

        }
    }
}

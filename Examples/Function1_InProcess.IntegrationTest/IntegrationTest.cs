using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Examples.Function1_InProcess;
using Microsoft.Extensions.Hosting;
using System.Net.Http;
using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Test;

namespace Examples.Test
{
    public class IntegrationLogic : ILogic
    {
        public string Text { get; set; }

        public IntegrationLogic()
        {
            Text = "Integration";
        }
    }

    // We only spin up the host so we can get the propper services from it
    // right now Azure functions can't be tested properly. There is some work going on to get a proper test suite as with WebApplicationFactory.
    //
    // Read more here: https://github.com/Azure/azure-functions-dotnet-worker/issues/281
    //
    // for now this is the best i can come up with. I dont think this is a good solution:
    // 1. we dont test the setup of the services
    // 2. with the new set out of process features (this is not out of process), we can actually inject middleware, which doesnt gets validated here (the runtime of in proces and out of process is the same so the test suite will be to)
    public class IntegrationTest
        : IClassFixture<CustomGenericHostApplicationFactory<TestHttpTrigger>>
    {
        private readonly CustomGenericHostApplicationFactory<TestHttpTrigger> _factory;

        public IntegrationTest(CustomGenericHostApplicationFactory<TestHttpTrigger> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task CallHttpTrigger()
        {
            var host = _factory.BuildHost((configuration) =>
            {
                configuration.ConfigureServices((context, services) =>
                {

                });

                configuration.ConfigureLogging((context, loggingBuilder) =>
                {
                    loggingBuilder.AddConsole();
                });

                var startup = new Startup();
                configuration.ConfigureWebJobs(startup.Configure);
            });

            var request = new DefaultHttpContext().Request;
            var testHttpTrigger = new TestHttpTrigger();
            var result = (OkObjectResult)(await testHttpTrigger.Run(request, host.Services.GetRequiredService<ILogger<TestHttpTrigger>>()));
            Assert.Equal("Ok", result.Value);
        }

        [Fact]
        public async Task CallQueueTrigger()
        {
            var host = _factory.BuildHost((configuration) =>
            {
                // Order matters. Startup config needs to be first, else it doesnt get overridden

                var startup = new Startup();
                configuration.ConfigureWebJobs(startup.Configure);

                configuration.ConfigureServices((context, services) =>
                {
                    var descriptor = services.SingleOrDefault(
                   d => d.ServiceType ==
                       typeof(ILogic));

                    services.Remove(descriptor);

                    services.AddScoped<ILogic, IntegrationLogic>();
                });

                configuration.ConfigureLogging((context, loggingBuilder) =>
                {
                    loggingBuilder.AddConsole();
                });

            });

            TestQueueTrigger testQueueTrigger = new TestQueueTrigger(host.Services.GetRequiredService<ILogic>());
            await testQueueTrigger.Run(new QueueMessageItem { Text = "Integration run" }, host.Services.GetRequiredService<ILogger<TestQueueTrigger>>());
        }
    }
}

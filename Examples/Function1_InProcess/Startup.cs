using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

[assembly: FunctionsStartup(typeof(Examples.Function1_InProcess.Startup))]

namespace Examples.Function1_InProcess
{
    // Right now the dependency injcetion only supports 3.x: https://docs.microsoft.com/en-us/azure/azure-functions/functions-dotnet-dependency-injection#prerequisites
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddHttpClient();
            builder.Services.AddScoped<ILogic, Logic>();
        }
    }
}

namespace Examples.Function1_InProcess
{
    public class Logic : ILogic
    {
        public string Text { get; set; }

        public Logic()
        {
            Text = "Production";
        }
    }
}

using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Examples.Function1_InProcess
{
    public class TestQueueTrigger
    {
        public ILogic Logic { get; }

        public TestQueueTrigger(ILogic logic)
        {
            Logic = logic;
        }

        [FunctionName("TestQueueTrigger")]
        public async Task Run(
        [QueueTrigger("queue")] QueueMessageItem item,
            ILogger log)
        {
            log.LogInformation($"Queue trigger triggered {item.Text} and {Logic.Text}");
        }
    }
}

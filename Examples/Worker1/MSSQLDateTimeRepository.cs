using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;

namespace Examples.Worker1
{
    public class MSSQLDateTimeRepository : IDateTimeRepository
    {
        private readonly DatabaseOptions _databaseOptions;

        public MSSQLDateTimeRepository(DatabaseOptions databaseOptions)
        {
            _databaseOptions = databaseOptions;
        }

        public IEnumerable<DateTimeOffset> GetAll()
        {
            using (var connection = new SqlConnection(_databaseOptions.ConnectionString))
            {
                return connection.QueryMultiple("SELECT * FROM TimeStamp").Read<DateTimeOffset>();
            }
        }

        public DateTimeOffset Upsert(DateTimeOffset timestamp)
        {
            DateTimeOffset returnedStamp;
            using (var connection = new SqlConnection(_databaseOptions.ConnectionString))
            {
                returnedStamp = connection.ExecuteScalar<DateTimeOffset>("INSERT INTO TimeStamp VALUES(@timestamp); SELECT @timestamp;", new { timestamp });
            }

            return returnedStamp;
        }
    }
}

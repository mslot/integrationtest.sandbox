using System;
using System.Collections.Generic;

namespace Examples.Worker1
{
    public interface IDateTimeRepository
    {
        IEnumerable<DateTimeOffset> GetAll();
        DateTimeOffset Upsert(DateTimeOffset timestamp);
    }
}

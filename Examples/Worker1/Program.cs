using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Examples.Worker1
{
    public class Program
    {
        // See more here for how to set up a workload: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-5.0#set-up-a-host
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    var databaseOptions = new DatabaseOptions();
                    var databaseSection = configuration.GetSection(nameof(DatabaseOptions));

                    services.AddSingleton<IDateTimeRepository, MSSQLDateTimeRepository>();
                    services.AddSingleton(databaseOptions);
                    services.AddHostedService<TestHostedService>();
                });
    }
}

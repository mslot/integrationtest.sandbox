-- we need to add the fully qualified table name and avoid USE, because of session switching in MSSQL: https://github.com/lecaillon/Evolve/issues/97
IF NOT EXISTS (
        SELECT 1
        FROM sys.tables
        WHERE name = 'TimeStamp'
            AND type = 'U'
        )
BEGIN
CREATE TABLE TimeStampDatabase.dbo.TimeStamp (
       timestamp datetimeoffset
)
END

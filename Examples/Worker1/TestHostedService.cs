using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Examples.Worker1
{
    public class TestHostedService : IHostedService
    {
        private IDateTimeRepository _repository;
        public TestHostedService(IDateTimeRepository repository)
        {
            _repository = repository;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _repository.Upsert(DateTimeOffset.Now);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}

using Examples.Worker1;
using Xunit;
using Test;
using System.Linq;
using System.Data.SqlClient;
using Microsoft.Extensions.DependencyInjection;

namespace Examples.Test
{
    public class IntegrationTest
        : IClassFixture<CustomGenericHostApplicationFactory<Program>>
    {
        private readonly CustomGenericHostApplicationFactory<Program> _factory;

        public IntegrationTest(CustomGenericHostApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Fact]
        public void SaveStamp()
        {
            var databaseConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("mssql"));

            // we add our own database that has been handed to us by the environment controller
            // we REMEMBER to set the correct initial catalog - in environment controller land we were on master, because it needs to create the database
            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
            connectionStringBuilder.Password = databaseConnectionString.Password;
            connectionStringBuilder.UserID = databaseConnectionString.Username;
            connectionStringBuilder.DataSource = $"{databaseConnectionString.Host},{databaseConnectionString.FirstPort}";

            _factory.StartHost((configuration) =>
            {
                configuration.ConfigureServices((context, services) =>
                {
                    // we need to remove the database options that has been setup
                    var databaseOptionsServiceDescriptor = services.FirstOrDefault(descriptor => descriptor.ServiceType == typeof(DatabaseOptions));
                    services.Remove(databaseOptionsServiceDescriptor);

                    services.AddSingleton(new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString });
                });
            });

            var repository = new MSSQLDateTimeRepository(new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString });

            var all = repository.GetAll();

            Assert.Equal(1, all.Count());
        }
    }
}

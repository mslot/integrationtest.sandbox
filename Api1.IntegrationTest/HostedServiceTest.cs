using Api1;
using Xunit;
using Test;
using Microsoft.Extensions.Configuration;
using Core;
using MassTransit;
using System.Security.Authentication;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.Data.SqlClient;

namespace Tests
{
    // An IClassFixture is run before and after each test class (not method)
    public class HostedServiceTest
        : IClassFixture<CustomHostedServiceFactory<Startup>>
    {
        private readonly CustomHostedServiceFactory<Startup> _factory;

        public HostedServiceTest(CustomHostedServiceFactory<Startup> factory)
        {
            Console.WriteLine("Run HostedServiceTest");
            _factory = factory;
        }

        [Fact]
        public async Task Testing_TestServer()
        {
            var messageQueueConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("rabbitmq"));
            var databaseConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("mssql"));

            await _factory.StartHost((builder) =>
            {
                builder.ConfigureAppConfiguration((context, configuration) =>
                {
                    // we inject rabbitmq into the configuration
                    // this needs to be done this way, because MassTransit is set up in Startup, and therefore doesnt get injected into the IoC
                    configuration.AddInMemoryCollection(new Dictionary<string, string>
                    {
                         {"MessageQueueOptions:Username",messageQueueConnectionString.Username},
                         {"MessageQueueOptions:Password",messageQueueConnectionString.Password},
                         {"MessageQueueOptions:Protocol",messageQueueConnectionString.Protocol},
                         {"MessageQueueOptions:Path",messageQueueConnectionString.Path},
                         {"MessageQueueOptions:Host",messageQueueConnectionString.Host},
                         {"MessageQueueOptions:Port",messageQueueConnectionString.FirstPort.ToString()}
                    });
                });
                builder.ConfigureServices((context, services) =>
                {
                    // we need to remove the database options that has been setup by the apis Startup
                    var databaseOptionsServiceDescriptor = services.FirstOrDefault(descriptor => descriptor.ServiceType == typeof(DatabaseOptions));
                    services.Remove(databaseOptionsServiceDescriptor);

                    // we add our own database that has been handed to us by the environment controller
                    // we REMEMBER to set the correct initial catalog - in environment controller land we were on master, because it needs to create the database
                    var connectionStringBuilder = new SqlConnectionStringBuilder();
                    connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
                    connectionStringBuilder.Password = databaseConnectionString.Password;
                    connectionStringBuilder.UserID = databaseConnectionString.Username;
                    connectionStringBuilder.DataSource = $"{databaseConnectionString.Host},{databaseConnectionString.FirstPort}";
                    connectionStringBuilder.TrustServerCertificate = true;

                    services.AddSingleton(new DatabaseOptions { ConnectionString = connectionStringBuilder.ConnectionString });
                });
            });

            // we need a bus so we can create a client that talks with the bus
            string host = $"rabbitmq://{messageQueueConnectionString.Host}:{messageQueueConnectionString.FirstPort}";

            var bus = Bus.Factory.CreateUsingRabbitMq((cfg) =>
            {
                if (!String.IsNullOrEmpty(messageQueueConnectionString.Path))
                {
                    host = $"{host}{messageQueueConnectionString.Path}";
                }

                cfg.Host(host, h =>
                {
                    h.Password(messageQueueConnectionString.Password);
                    h.Username(messageQueueConnectionString.Username);

                    if (messageQueueConnectionString.Protocol.Equals("tls12", StringComparison.InvariantCulture))
                    {
                        h.UseSsl(s =>
                    {
                        s.Protocol = SslProtocols.Tls12;
                    });
                    }
                });
            });

            bus.Start();

            var client = await bus.GetSendEndpoint(new Uri("queue:atimestampqueue"));
            var timestamp = DateTimeOffset.Now;

            // Send some data to the hosted service
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));
            await client.Send<DateTimeStampMessage>(new DateTimeStampMessage(timestamp));


            var repo = _factory.Services.GetService<IDateTimeRepository>();

            // See if we have the rows
            int tryCount = 0;
            int foundRows = 0;

            while (true)
            {
                var rows = repo.GetAll();

                foundRows = rows.Count();
                if (tryCount == 3)
                {
                    break;
                }

                await Task.Delay(TimeSpan.FromSeconds(20));
                tryCount++;
            }

            Assert.Equal(9, foundRows);
            await _factory.StopHost();
        }
    }
}

using Core;
using System.Net.Http.Json;
using Api1;
using Api1.IntegrationTest;
using MassTransit;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using Test;
using Microsoft.Extensions.Configuration;
using System.Security.Authentication;
using System.Text.Encodings.Web;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace Tests
{
    // An IClassFixture is run before and after each test class (not method)
    public class IntegrationTest
        : IClassFixture<CustomWebApplicationFactoryBase<Program>>, IDisposable
    {
        private readonly CustomWebApplicationFactoryBase<Program> _factory;

        public IntegrationTest(CustomWebApplicationFactoryBase<Program> factory)
        {
            Console.WriteLine("Run IntegrationTest");
            _factory = factory;
        }

        public void Dispose()
        {
            _factory.Dispose();
        }

        [InlineData("/timestamp")]
        [Theory]
        public async Task Post_TimestampWithNoDateTime(string url)
        {
            // we find the correct connectionstring
            var messageQueueConnectionString = _factory.ConnectionStrings.First(c => c.Identifier.Equals("rabbitmq"));
            Console.WriteLine($"Integration: {messageQueueConnectionString.Host}{messageQueueConnectionString.Path}{messageQueueConnectionString.FirstPort.ToString()}");

            // Arrange
            var customFactory = _factory.WithWebHostBuilder(builder =>
            {
                // prior to .NET Core 3, ConfigureTestServices was executed before the SUTs ConfigureServices (and .NET Core < 3 have a ConfigureTestServices
                // that was executed after the SUTs ConfigureServices). This has changed, and is a breaking change in .NET Core 3.
                // Read more in this paragraph: https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-5.0#customize-webapplicationfactory
                builder.ConfigureTestServices(services =>
                {
                })
                .ConfigureAppConfiguration((context, configuration) =>
                {
                    // we inject rabbitmq into the configuration
                    // this needs to be done this way, because MassTransit is set up in Startup, and therefore doesnt get injected into the IoC
                    configuration.AddInMemoryCollection(new Dictionary<string, string>
                    {
                        {"MessageQueueOptions:Username",messageQueueConnectionString.Username},
                        {"MessageQueueOptions:Password",messageQueueConnectionString.Password},
                        {"MessageQueueOptions:Protocol",messageQueueConnectionString.Protocol},
                        {"MessageQueueOptions:Host",messageQueueConnectionString.Host},
                        {"MessageQueueOptions:Path",messageQueueConnectionString.Path},
                        {"MessageQueueOptions:Port",messageQueueConnectionString.FirstPort.ToString()}
                    });
                });
            });

            var client = customFactory.CreateClient();

            // we need a fake receive endpoint
            // here the type of the consumed type is the name of the rabbitmq exchange, and the runtime just binds the "myqeueuname" to the exchange
            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                        {
                            string host = $"rabbitmq://{messageQueueConnectionString.Host}:{messageQueueConnectionString.FirstPort}";

                            if (!String.IsNullOrEmpty(messageQueueConnectionString.Path))
                            {
                                host = $"{host}{messageQueueConnectionString.Path}";
                            }

                            cfg.Host(host, h =>
                            {
                                h.Password(messageQueueConnectionString.Password);
                                h.Username(messageQueueConnectionString.Username);

                                if (messageQueueConnectionString.Protocol.Equals("tls12", StringComparison.InvariantCulture))
                                {
                                    h.UseSsl(s =>
                                    {
                                        s.Protocol = SslProtocols.Tls12;
                                    });
                                }
                            });
                            cfg.ReceiveEndpoint("myqueuename", e =>
                            {
                                e.Consumer<FakeDateTimeStampConsumer>();
                            });
                        });

            await busControl.StartAsync();

            // Act
            var response = await client.PostAsJsonAsync(url, new DateTimeStampMessage());

            // Assert
            var dateTimeResponse = await response.Content.ReadFromJsonAsync<DateTimeResponse>();
            Assert.NotNull(dateTimeResponse);

            await busControl.StopAsync();
        }
    }
}

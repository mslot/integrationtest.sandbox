using Core;
using System;
using System.Threading.Tasks;
using Api1;
using MassTransit;

namespace Api1.IntegrationTest
{
    public class FakeDateTimeStampConsumer : IConsumer<DateTimeStampMessage>
    {
        public async Task Consume(ConsumeContext<DateTimeStampMessage> context)
        {
            await context.RespondAsync(new DateTimeResponse(context.Message.Stamp));
        }
    }
}

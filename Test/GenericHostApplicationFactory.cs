using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Test
{
    // This is my poor atempt at creating something that can be used in a integration test, inspired by WebApplicationFactory.
    //
    // You can find the real source here: https://github.com/dotnet/aspnetcore/blob/8f871c4e08a25b522da7eb7a9579c3249d653326/src/Mvc/Mvc.Testing/src/WebApplicationFactory.cs
    // I have collected the HostFactoryResolver and bits from the WebApplicationFactory together in here
    //
    // This is a minimal MVP for what I need, so a lot is missing (if you compare it with WebApplicationFactory)

    public class GenericHostApplicationFactory<T> : IDisposable, IAsyncDisposable where T : class
    {
        private const BindingFlags DeclaredOnlyLookup = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
        private bool _disposedValue;
        private bool _disposedAsync;
        private IHost _host;

        protected virtual IHostBuilder CreateHostBuilder()
        {
            var assembly = typeof(T).Assembly;
            var builder = ResolveFactory<IHostBuilder>(assembly, "CreateHostBuilder");

            if (builder != null)
            {
                var hostBuilder = builder.Invoke(Array.Empty<string>());

                return hostBuilder;
            }

            // returns a new clean host
            return new HostBuilder();
        }

        public virtual IHost BuildHost(Action<IHostBuilder> configuration)
        {
            var hostBuilder = CreateHostBuilder();
            hostBuilder = ConfigureHostBuilder(configuration, hostBuilder);
            _host = hostBuilder.Build();
            return _host;
        }

        public virtual void StartHost(Action<IHostBuilder> configuration)
        {
            _host = BuildHost(configuration);
            _host.Start();
        }

        public virtual IHostBuilder ConfigureHostBuilder(Action<IHostBuilder> configuration, IHostBuilder hostBuilder)
        {
            hostBuilder.UseEnvironment("development");
            hostBuilder.ConfigureServices((context, services) =>
            {
            });

            configuration(hostBuilder);

            return hostBuilder;
        }

        private static Func<string[], U> ResolveFactory<U>(Assembly assembly, string name)
        {
            var programType = assembly?.EntryPoint?.DeclaringType;
            if (programType == null)
            {
                return null;
            }

            var factory = programType.GetMethod(name, DeclaredOnlyLookup);
            if (!IsFactory<U>(factory))
            {
                return null;
            }

            return args => (U)factory!.Invoke(null, new object[] { args })!;
        }

        private static bool IsFactory<TReturn>(MethodInfo factory)
        {
            return factory != null
                && typeof(TReturn).IsAssignableFrom(factory.ReturnType)
                && factory.GetParameters().Length == 1
                && typeof(string[]).Equals(factory.GetParameters()[0].ParameterType);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    DisposeAsync()
                        .AsTask()
                        .ConfigureAwait(false)
                        .GetAwaiter()
                        .GetResult();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public virtual async ValueTask DisposeAsync()
        {
            if (_disposedValue)
            {
                return;
            }

            if (_disposedAsync)
            {
                return;
            }

            await _host.StopAsync().ConfigureAwait(false);
            _host.Dispose();

            _disposedAsync = true;

            Dispose(disposing: true);

            GC.SuppressFinalize(this);
        }
    }
}

using Ductus.FluentDocker.Builders;
using Ductus.FluentDocker.Services;
using Ductus.FluentDocker.Services.Extensions;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;
using Azure.Storage.Blobs;
using Azure.Storage.Queues;
using Azure.Data.Tables;
using HareDu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net.Http;
using System.Threading;
using Microsoft.Data.SqlClient;

namespace Test
{
    // This is a PoC of how all the bits and pieces could look for how to spin up a test environment - i have tried not to "libraryfy" or "frameworkify" anything so the API can evolve and it is easy to understand
    // I have only modelled on three major techs: rabbitmq, mssql and azure storage, because: if it, for whatever reason, couldnt be done the way i wanted, with these, then i needed to change tactics
    //
    // Instead of the Console.WriteLines here, we should really use https://www.nuget.org/packages/MartinCostello.Logging.XUnit/ - but that is another post
    //
    // Read more here: https://xunit.net/docs/shared-context
    public class EnvironmentController : IDisposable
    {
        private IContainerService _azureiteContainer;
        private IContainerService _rabbitmqContainer;
        private IContainerService _mssqlContainer;
        private ICompositeService _compose;
        private bool disposedValue;

        public List<ConnectionString> ConnectionStrings { get; } = new List<ConnectionString>();

        public void Setup()
        {
            var prefix = Guid.NewGuid().ToString().Replace("-", String.Empty);

            Console.WriteLine(prefix);
            _compose = new Builder()
                              .UseContainer()
                              .UseCompose()
                              .ServiceName(prefix) // we want a random container name and network name
                              .FromFile("test-env.yaml")
                              .RemoveOrphans()
                              .Build().Start();

            string saPassword = "YourStrong(!)Password^2";

            _mssqlContainer = _compose.Containers.First(c => c.Name.Equals("mssql-database"));

            var mssqlPort = _mssqlContainer.ToHostExposedEndpoint("1433/tcp").Port;
            var mssqlHost = GetHostFromEnvironment();

            _rabbitmqContainer = _compose.Containers.First(c => c.Name.Equals("rabbitmq"));
            var rabbitmqPort = _rabbitmqContainer.ToHostExposedEndpoint("5672/tcp").Port;
            var rabbitmqManagementPort = _rabbitmqContainer.ToHostExposedEndpoint("15672/tcp").Port;
            var rabbitmqHostString = _rabbitmqContainer.ToHostExposedEndpoint("5672/tcp").Address.ToString();
            var rabbitmqHost = GetHostFromEnvironment();

            _azureiteContainer = _compose.Containers.First(c => c.Name.Equals("azurecontainer"));
            var azureStorageBlobPort = _azureiteContainer.ToHostExposedEndpoint("10000/tcp").Port;
            var azureStorageQueuePort = _azureiteContainer.ToHostExposedEndpoint("10001/tcp").Port;
            var azureStorageHost = GetHostFromEnvironment();

            var mssqlConnectionString = BuildMssqlConnectionString(mssqlHost, mssqlPort, saPassword);
            var rabbitmqConnectionString = BuildRabbitmqConnectionString(rabbitmqHost, rabbitmqPort);
            var rabbitmqManagementConnectionString = BuildRabbitmqManagementConnectionString(rabbitmqHost, rabbitmqManagementPort);
            var azureStorageConnectionString = BuildAzureStorageConnectionString(azureStorageHost, azureStorageBlobPort, azureStorageQueuePort);

            ConnectionStrings.Add(mssqlConnectionString);
            ConnectionStrings.Add(rabbitmqConnectionString);
            ConnectionStrings.Add(azureStorageConnectionString);
            ConnectionStrings.Add(rabbitmqManagementConnectionString);

            WaitForRabbitmqToBeReady(rabbitmqConnectionString);
            WaitForMsSqlToBeReady(mssqlConnectionString);
            WaitForAzureStorageToBeReady(azureStorageConnectionString);
            RunMigrations(mssqlConnectionString);
        }

        // Basic clean - it deletes all things an recreate them
        // Not all here is tested - only MSSQL. In my current setup i dont need to reuse storage accounts and rabbitmq
        public void Clean()
        {
            var tableNames = new List<string>();
            CleanAzureStorage(tableNames);
            CleanMssql();
            CleanRabbitmqQueues();
        }

        // this only covers two scenarios:
        // 1. if we are on localhost, and DOCKER_HOST isn't set to docker: return localhost, else
        // 2. if we are on gitlab, return docker (that is the dind default hostname)
        private string GetHostFromEnvironment()
        {
            string hostEnvironmentVariable = Environment.GetEnvironmentVariable("DOCKER_HOST");

            if (!String.IsNullOrEmpty(hostEnvironmentVariable) &&
            hostEnvironmentVariable.Contains("docker"))
            {
                return "docker";
            }

            return "localhost";
        }

        private ConnectionString BuildAzureStorageConnectionString(string host, int blobPort, int queuePort)
        {
            return new ConnectionString("azureStorage", String.Empty, String.Empty, "http", host, string.Empty,
                                        new Dictionary<string, int> { { "blob", blobPort }, { "queue", queuePort } });
        }

        private ConnectionString BuildRabbitmqManagementConnectionString(string host, int port)
        {
            return new ConnectionString("rabbitmq-management", "guest", "guest", "http", host, string.Empty, new Dictionary<string, int> { { "rabbitmq-management", port } });
        }

        private ConnectionString BuildRabbitmqConnectionString(string host, int port)
        {
            return new ConnectionString("rabbitmq", "guest", "guest", "tcp", host, string.Empty, new Dictionary<string, int> { { "rabbitmq", port } });
        }

        private ConnectionString BuildMssqlConnectionString(string host, int port, string password)
        {
            return new ConnectionString("mssql", "sa", password, "tcp", host, String.Empty, new Dictionary<string, int> { { "mssql", port } });
        }

        private void RunMigrations(ConnectionString connectionString)
        {
            if (Directory.Exists("db/migrations/initial"))
            {
                Console.WriteLine("Creating database");
                var connectionStringBuilder = new SqlConnectionStringBuilder();
                connectionStringBuilder.InitialCatalog = "master";
                connectionStringBuilder.Password = connectionString.Password;
                connectionStringBuilder.UserID = connectionString.Username;
                connectionStringBuilder.DataSource = $"{connectionString.Host},{connectionString.FirstPort}";
                connectionStringBuilder.TrustServerCertificate = true;

                var connectionCreateDatabase = new SqlConnection(connectionStringBuilder.ConnectionString);
                var evolveCreateDatabase = new Evolve.Evolve(connectionCreateDatabase, s => { Console.WriteLine(s); })
                {
                    Locations = new[] { "db/migrations/initial" },
                    Schemas = new List<string> { "dbo" }
                };

                evolveCreateDatabase.Migrate();

                Console.WriteLine("Adding migrations to TimeStampDatabase");

                connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
                var connection = new SqlConnection(connectionStringBuilder.ConnectionString);
                var evolve = new Evolve.Evolve(connection, s => { Console.WriteLine(s); })
                {
                    Locations = new[] { "db/migrations/timestamp_database" },
                    Schemas = new List<string> { "dbo" }
                };

                evolve.Migrate();
                Console.WriteLine("Migrations ran succesfully");
            }
            else
            {
                Console.WriteLine("No initial dabatabase files found - skipping database creation");
            }
        }

        private void CleanRabbitmqQueues()
        {
            var connectionString = ConnectionStrings.FirstOrDefault(c => c.Identifier.Equals("rabbitmq-management"));
            using (var httpClient = new HttpClient()
            {
                BaseAddress = new Uri($"http://{connectionString.Username}:{connectionString.Password}@{connectionString.Host}:{connectionString.FirstPort}")
            })
            {
                var queues = new BrokerObjectFactory(httpClient)
                    .Object<Queue>()
                    .GetAll().GetAwaiter().GetResult();

                foreach (var queue in queues.Data)
                {
                    new BrokerObjectFactory(httpClient)
                        .Object<Queue>()
                        .Empty(queue.Name, connectionString.Path)
                        .GetAwaiter()
                        .GetResult();

                    Console.WriteLine($"Emptied rabbitmq queue: {queue.Name}");
                }
            }
        }

        // Maybe pass in the initial catalog for better reuse?
        private void CleanMssql()
        {
            var connectionString = ConnectionStrings.FirstOrDefault(c => c.Identifier.Equals("mssql"));

            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.InitialCatalog = "TimeStampDatabase";
            connectionStringBuilder.Password = connectionString.Password;
            connectionStringBuilder.UserID = connectionString.Username;
            connectionStringBuilder.DataSource = $"{connectionString.Host},{connectionString.FirstPort}";
            connectionStringBuilder.TrustServerCertificate = true;

            var connection = new SqlConnection(connectionStringBuilder.ConnectionString);
            var evolve = new Evolve.Evolve(connection, s => { Console.WriteLine(s); })
            {
                Schemas = new List<string> { "dbo" }
            };

            evolve.Erase();

            RunMigrations(connectionString);

            Console.WriteLine("Recreated MSSQL");
        }

        // Azurite doesnt support fileshares: https://github.com/Azure/Azurite/issues/113
        // For now i dont support it here either (by omitting it from connectionstring) - somehow it is also not possible to get all tables, like the blob and queue,
        // so you have to pass the names when clearing
        //
        // I actually dont know why i cant clear queues etc with the new clients. You could with the old ones, but they have been marked as deprecated
        private void CleanAzureStorage(IEnumerable<string> tableNames)
        {
            var connectionString = ConnectionStrings.FirstOrDefault(c => c.Identifier.Equals("azureStorage"));
            string azureConnectionString = $"DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;QueueEndpoint=http://{connectionString.Host}:{connectionString.Ports["queue"]}/devstoreaccount1;BlobEndpoint=http://{connectionString.Host}:{connectionString.Ports["blob"]}/devstoreaccount1";
            BlobServiceClient blobService = new BlobServiceClient(azureConnectionString);
            QueueServiceClient queueService = new QueueServiceClient(azureConnectionString);
            TableServiceClient tableService = new TableServiceClient(azureConnectionString);

            foreach (var container in blobService.GetBlobContainers())
            {
                blobService.DeleteBlobContainer(container.Name);
                blobService.CreateBlobContainer(container.Name);
            }

            foreach (var queue in queueService.GetQueues())
            {
                queueService.DeleteQueue(queue.Name);
                queueService.CreateQueue(queue.Name);
            }

            foreach (var tableName in tableNames)
            {
                tableService.DeleteTable(tableName);
                tableService.CreateTable(tableName);
            }
        }

        private void WaitForAzureStorageToBeReady(ConnectionString connectionString)
        {
            string azureConnectionString = $"DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;QueueEndpoint=http://{connectionString.Host}:{connectionString.Ports["queue"]}/devstoreaccount1;BlobEndpoint=http://{connectionString.Host}:{connectionString.Ports["blob"]}/devstoreaccount1";
            BlobServiceClient service = new BlobServiceClient(azureConnectionString);

            while (true)
            {
                try
                {
                    service.GetProperties();
                    Console.WriteLine("Azure storage is up");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("didnt connect to azure storage... waiting 5 secs");
                    Thread.Sleep(5000);
                }
            }
        }

        private void WaitForMsSqlToBeReady(ConnectionString connectionString)
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.InitialCatalog = "master";
            connectionStringBuilder.Password = connectionString.Password;
            connectionStringBuilder.UserID = connectionString.Username;
            connectionStringBuilder.DataSource = $"{connectionString.Host},{connectionString.FirstPort}";
            connectionStringBuilder.TrustServerCertificate = true;

            var connection = new SqlConnection(connectionStringBuilder.ConnectionString);

            while (true)
            {
                try
                {
                    connection.Open();
                    Console.WriteLine("MSSQL is up");
                    break;

                }
                catch (SqlException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("didnt connect to MSSQL... waiting 5 secs");
                    Thread.Sleep(5000);
                }
            }

            connection.Close();
        }

        private void WaitForRabbitmqToBeReady(ConnectionString connectionString)
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.UserName = connectionString.Username;
            factory.Password = connectionString.Password;
            factory.Port = connectionString.FirstPort;
            factory.HostName = connectionString.Host;

            IConnection connection = null;

            Console.WriteLine(connectionString.Host + " " + connectionString.FirstPort + " " + connectionString.Username + " " + connectionString.Password);

            while (true)
            {
                try
                {
                    connection = factory.CreateConnection();
                    Console.WriteLine("Rabbitmq is up");
                    break;
                }
                catch (BrokerUnreachableException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(System.DateTime.Now + ": didnt connect to Rabbitmq... waiting 5 secs");
                    Thread.Sleep(5000);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something else happened: " + e.Message);
                    break;
                }
                finally
                {
                    connection?.Close();
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _compose.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

namespace Test
{
    public class GenericHostedServiceFactory<T> : IDisposable, IAsyncDisposable where T : class
    {
        private const BindingFlags DeclaredOnlyLookup = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
        private bool _disposedValue;
        private bool _disposedAsync;
        protected IHost _host;
        private bool disposedValue;
        public IServiceProvider Services { get; set; }

        protected virtual IHostBuilder CreateHostBuilder()
        {
            var assembly = typeof(T).Assembly;
            var builder = ResolveFactory<IHostBuilder>(assembly, "CreateHostBuilder");

            if (builder != null)
            {
                var hostBuilder = builder.Invoke(Array.Empty<string>());

                return hostBuilder;
            }

            // returns a new clean host
            return new HostBuilder();
        }

        public virtual async Task StartHost(Action<IHostBuilder> configuration)
        {
            var hostBuilder =
                new HostBuilder()
                .ConfigureWebHost(builder =>
            {
                builder.UseTestServer();
                builder.UseStartup<T>();
            });

            configuration(hostBuilder);

            _host = await hostBuilder.StartAsync();
            Services = _host.Services;
        }

        public virtual async Task StopHost()
        {
            await _host.StopAsync();
        }

        private static Func<string[], U> ResolveFactory<U>(Assembly assembly, string name)
        {
            var programType = assembly?.EntryPoint?.DeclaringType;
            if (programType == null)
            {
                return null;
            }

            var factory = programType.GetMethod(name, DeclaredOnlyLookup);
            if (!IsFactory<U>(factory))
            {
                return null;
            }

            return args => (U)factory!.Invoke(null, new object[] { args })!;
        }

        private static bool IsFactory<TReturn>(MethodInfo factory)
        {
            return factory != null
                && typeof(TReturn).IsAssignableFrom(factory.ReturnType)
                && factory.GetParameters().Length == 1
                && typeof(string[]).Equals(factory.GetParameters()[0].ParameterType);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    DisposeAsync()
                        .AsTask()
                        .ConfigureAwait(false)
                        .GetAwaiter()
                        .GetResult();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public virtual async ValueTask DisposeAsync()
        {
            if (_disposedValue)
            {
                return;
            }

            if (_disposedAsync)
            {
                return;
            }

            await _host.StopAsync().ConfigureAwait(false);
            _host.Dispose();

            _disposedAsync = true;

            Dispose(disposing: true);

            GC.SuppressFinalize(this);
        }
    }
}

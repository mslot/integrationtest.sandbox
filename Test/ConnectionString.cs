using System.Collections.Generic;
using System.Linq;

namespace Test
{
    public class ConnectionString
    {
        public string Identifier { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Protocol { get; private set; }
        public string Host { get; private set; }
        public string Path { get; private set; }
        public Dictionary<string, int> Ports { get; private set; }
        public int FirstPort { get { return Ports.Values.FirstOrDefault(); } }

        public ConnectionString(string identifier, string username, string password, string protocol, string host, string path, Dictionary<string, int> ports)
        {
            Identifier = identifier;
            Username = username;
            Password = password;
            Protocol = protocol;
            Host = host;
            Path = path;
            Ports = ports;
        }
    }
}

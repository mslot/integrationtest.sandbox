using System.Collections.Generic;

namespace Test
{
    public class CustomHostedServiceFactory<T> : GenericHostedServiceFactory<T> where T : class
    {
        // the environment controller could be moved out to a fixture it self, as explained here https://xunit.net/docs/shared-context - quote "If you need multiple fixture objects, you can implement the interface as many times as you want, and add constructor
        // arguments for whichever of the fixture object instances you need access to. The order of the constructor arguments is unimportant. "
        // but for now, i have it here - easier in a PoC
        private EnvironmentController _environmentController = new EnvironmentController();
        public IEnumerable<ConnectionString> ConnectionStrings { get { return _environmentController.ConnectionStrings; } }
        private bool _disposedValue;

        public CustomHostedServiceFactory()
        {
            _environmentController.Setup();
        }

        // this is called by the IClassFixture logic in xUnit: https://xunit.net/docs/shared-context - quote:"When using a class fixture, xUnit.net will ensure that the fixture instance will be created before any of the tests have run,
        // and once all the tests have finished, it will clean up the fixture object by calling Dispose, if present."
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!_disposedValue)
            {
                if (disposing)
                {
                    _environmentController.Dispose();
                }

                _disposedValue = true;
            }
        }
    }
}

using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Test
{
    public class CustomWebApplicationFactoryBase<T>
        : WebApplicationFactory<T> where T : class
    {
        // the environment controller could be moved out to a fixture it self, as explained here https://xunit.net/docs/shared-context - quote "If you need multiple fixture objects, you can implement the interface as many times as you want, and add constructor
        // arguments for whichever of the fixture object instances you need access to. The order of the constructor arguments is unimportant. "
        // but for now, i have it here - easier in a PoC
        private EnvironmentController _environmentController;
        public IEnumerable<ConnectionString> ConnectionStrings { get { return _environmentController.ConnectionStrings; } }

        public CustomWebApplicationFactoryBase()
        {
            _environmentController = new EnvironmentController();
            _environmentController.Setup();
        }

        protected override IHostBuilder CreateHostBuilder()
        {
            // This isn't documented in the docs, but by using api1.Program, the base.CreateBuilder looks for the SUT assembly entry point (the CreateHostBuilder(string[] args))
            // See: https://github.com/dotnet/aspnetcore/blob/71dd6b0c87d7619668a40876d084d93db57eab41/src/Mvc/Mvc.Testing/src/WebApplicationFactory.cs#L370
            var builder = base.CreateHostBuilder();

            builder.ConfigureServices(services =>
            {
                services.Configure<HostOptions>(hostOptions =>
                        {
                            // Prior to .NET 6, the hosted services just shutdown, and no error was rethrown. This has been solved, but we want the exception to be ignored when
                            // doing integration test.
                            //
                            // Read more here: https://docs.microsoft.com/en-us/dotnet/core/compatibility/core-libraries/6.0/hosting-exception-handling
                            hostOptions.BackgroundServiceExceptionBehavior = BackgroundServiceExceptionBehavior.Ignore;
                        });
            });

            return builder;
        }

        // Before .NET 6, the Host.StopAsync wasn't called in base.Dispose, making undefined shutdown when fx MassTransit wanted to shutdown gracefully.
        // This has now been fixed in .NET 6 WebApplicationFactory :)
        //
        // this is called by the IClassFixture logic in xUnit: https://xunit.net/docs/shared-context - quote:"When using a class fixture, xUnit.net will ensure that the fixture instance will be created before any of the tests have run,
        // and once all the tests have finished, it will clean up the fixture object by calling Dispose, if present."
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _environmentController.Dispose();
        }
    }

}

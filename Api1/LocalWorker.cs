using System;
using System.Threading;
using System.Threading.Tasks;
using Core;
using MassTransit;
using MassTransit.Registration;
using Microsoft.Extensions.Hosting;

namespace Api1
{
    public class LocalWorker : IHostedService, IDisposable
    {
        private Timer _timer = null;
        private readonly IHostedServiceBus _hostedServiceBus;
        private IBusInstance<IHostedServiceBus> _instance;
        private readonly IDateTimeRepository _repository;

        public LocalWorker(IHostedServiceBus hostedServiceBus,
                           IBusInstance<IHostedServiceBus> instance,
                           IDateTimeRepository repository)
        {
            _hostedServiceBus = hostedServiceBus;
            _instance = instance;
            _repository = repository;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("LocalWorker started");
            _timer = new Timer(DoWork, null, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(60));
            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            try
            {
                _instance.BusControl.Start();
                _instance.BusControl.CheckHealth();
            }
            catch (Exception e)
            {
                Console.WriteLine("error starting");
            }

            Console.WriteLine("Started Local Worker");

            var handler = _instance.Bus.ConnectReceiveEndpoint("atimestampqueue", x =>
            {
                x.Handler<DateTimeStampMessage>(context =>
                {
                    _repository.Upsert(context.Message.Stamp);

                    return Task.CompletedTask;
                });
            });

            await handler.Ready;

            Console.WriteLine("Stopping local worker");
            await handler.StopAsync();

            try
            {
                _instance.BusControl.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine("error stopping");
            }

            Console.WriteLine("Stopped local worker");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("LocalWorker stopped");
            _timer.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}

namespace Api1
{
    public interface ILogic
    {
        string CalculateValue();
    }

    public class DefaultLogic : ILogic
    {
        public string CalculateValue()
        {
            return "Production pong";
        }
    }

}

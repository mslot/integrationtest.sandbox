using System;
using System.Collections.Generic;

namespace Api1
{
    public interface IDateTimeRepository
    {
        IEnumerable<DateTimeOffset> GetAll();
        DateTimeOffset Upsert(DateTimeOffset timestamp);
    }
}

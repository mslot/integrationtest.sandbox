using System;
using System.Security.Authentication;
using Core;
using MassTransit;
using MassTransit.MultiBus;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.OpenApi.Models;

namespace Api1
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // we do it this way because we dont want to be dependent on IOptions
            var messageQueueOptions = new MessageQueueOptions();
            DatabaseOptions databaseOptions = new DatabaseOptions();
            var messageQueueOptionsSection = Configuration.GetSection(nameof(MessageQueueOptions));
            var databaseSection = Configuration.GetSection(nameof(DatabaseOptions));

            messageQueueOptionsSection.Bind(messageQueueOptions);
            databaseSection.Bind(databaseOptions);

            services.AddSingleton(databaseOptions);
            services.AddSingleton<IDateTimeRepository, MSSQLDateTimeRepository>();

            services.AddHealthChecks();
            services.Configure<HealthCheckPublisherOptions>(options =>
            {
                options.Delay = TimeSpan.FromSeconds(2);
                options.Predicate = (check) => check.Tags.Contains("ready");
            });

            services.AddHostedService<LocalWorker>();
            services.AddControllers();

            services.AddMassTransit<IHostedServiceBus>(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.AutoStart = true;
                    string host = $"rabbitmq://{messageQueueOptions.Host}:{messageQueueOptions.Port}";

                    if (!String.IsNullOrEmpty(messageQueueOptions.Path))
                    {
                        host = $"{host}{messageQueueOptions.Path}";
                    }

                    cfg.Host(host, h =>
                    {
                        h.Password(messageQueueOptions.Password);
                        h.Username(messageQueueOptions.Username);

                    });

                    cfg.ReceiveEndpoint("test-queue", e =>
                    {
                        e.Handler<Message>(async context =>
                        {
                            await Console.Out.WriteAsync($"received {context.Message.Text}");
                        });
                    });
                });
            });

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    string host = $"rabbitmq://{messageQueueOptions.Host}:{messageQueueOptions.Port}";

                    if (!String.IsNullOrEmpty(messageQueueOptions.Path))
                    {
                        host = $"{host}{messageQueueOptions.Path}";
                    }

                    cfg.Host(host, h =>
                    {
                        h.Password(messageQueueOptions.Password);
                        h.Username(messageQueueOptions.Username);

                        if (messageQueueOptions.Protocol.Equals("tls12", StringComparison.InvariantCulture))
                        {
                            h.UseSsl(s =>
                            {
                                s.Protocol = SslProtocols.Tls12;
                            });
                        }
                    });
                });

                x.AddRequestClient<DateTimeStampMessage>();
                services.AddMassTransitHostedService();

                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "api1", Version = "v1" });
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "api1 v1"));
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
                        {
                            endpoints.MapHealthChecks("/health/ready", new HealthCheckOptions()
                            {
                                Predicate = (check) => check.Tags.Contains("ready"),
                            });

                            endpoints.MapHealthChecks("/health/live", new HealthCheckOptions());
                        });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

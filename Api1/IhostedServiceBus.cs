using MassTransit;

namespace Api1
{
    public interface IHostedServiceBus :
        IBus
    {
        IBusControl Control { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PingController : ControllerBase
    {
        private ILogic _logic;
        public PingController(ILogic logic)
        {
            _logic = logic;
        }

        [HttpGet]
        public async Task<string> Get()
        {
            return await Task.FromResult(_logic.CalculateValue());
        }
    }
}

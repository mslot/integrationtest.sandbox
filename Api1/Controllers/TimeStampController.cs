using System;
using System.Threading.Tasks;
using Core;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace Api1.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class TimeStampController : ControllerBase
    {
        private IRequestClient<DateTimeStampMessage> _client;

        public TimeStampController(IRequestClient<DateTimeStampMessage> client)
        {
            _client = client;
        }

        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> Post([FromBody] DateTimeStampMessage message)
        {
            Console.WriteLine("hit");
            // In a real world scenario this should be moved out to some logic, and be covered by an unit test, but for this showcase, this is fine
            if (message.Stamp == null || message.Stamp == DateTimeOffset.MinValue)
            {
                message.SetStampToNow();
            }

            var response = await _client.GetResponse<DateTimeResponse>(message);

            return Ok(response);
        }
    }
}

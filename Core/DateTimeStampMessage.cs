using System;

namespace Core
{
    public class DateTimeStampMessage
    {
        public DateTimeOffset Stamp { get; private set; }

        public DateTimeStampMessage()
        {
        }

        public DateTimeStampMessage(DateTimeOffset stamp)
        {
            Stamp = stamp;
        }

        public void SetStampToNow()
        {
            Stamp = DateTimeOffset.Now;
        }
    }
}

using System;

namespace Core
{
    public class DateTimeResponse
    {
        private DateTimeOffset _timestamp;

        public DateTimeResponse(DateTimeOffset timestamp)
        {
            _timestamp = timestamp;
        }

        public DateTimeOffset TimeStamp { get { return _timestamp; } }
    }
}
